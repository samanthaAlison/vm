#include <string>
#include <vector>

#include <iostream>
using std::cin; using std::cout; using std::cerr; using std::endl;
#include <iomanip>
#include <fstream>
#include <sstream>


#include <cmath>

#include <map>
#include <queue>

const long PHYSICAL_MEMORY_SIZE = 1048576;
const int FRAME_COUNT = 256;
const int LOGICAL_ADDRESS_LENGTH = 16;
const int FIRST_FRAME_NUMBER = 0x40;

class PageTable;
class PageReplacementSystem;

class VirtualMemorySystem
{
private:

    void ReadCommand(int page, int offset);
    void WriteCommand(int page, int offset);
    
    /// Amount of commands between each table print.
    /// A value of 0 means don't ever pint the table.
    int mTablePrintInterval = 0;

    /// Read command count
    int mReadCount = 0;
    /// Write command count
    int mWriteCount = 0;

    /// Physical memory we are working with, in bytes.
    long mPhysicalMemorySize;

    /// Memory address space we promise to each program, in bytes.
    long mVirtualAddressSpace;

    /// Number of page frames
    int mFrameCount;

    /// Logical address size, in bits. 
    long mLogicalAddressSize;

    /// Size of each page, in bytes.
    int mPageSize = 0;

    /// Page table that this system manages.
    PageTable * mPageTable;
    
    /// Page Replacement Manager
    PageReplacementSystem *  mPageReplacementSystem;

    /// Amount of bits used for the offset in virtual memory addresses.
    int mOffsetBitSize;
    
    /// Bit mask to use to extract the offset from a command.
    int mOffsetBitMask;

public:
    VirtualMemorySystem() {}
    VirtualMemorySystem(long pMemory, int pageFrames, int logicalAddressSize, int printInterval, PageReplacementSystem * pRSystem);
    ~VirtualMemorySystem();

    void DoCommand(char, int);

    /**
     * Getter for mPageTable.
     * @returns mPageTable. */
    PageTable * GetPageTable() { return mPageTable; }

     void SetReplacementSystem(PageReplacementSystem * system); 

};

/// Class that describes an entry in the page table.
class PageTableEntry
{
private:
    /// Flag that is set to indicate that this entry is valid.
    bool mValidBit = false;

    /// Flag that is set to indicate that this page that this entry represents has been 
    /// referenced.
    bool mReferenceBit = false;

    /// Flag that is set when the page this entry references has been modified. 
    bool mModifiedBit = false;


    /// Number of the of the virtual page that this table entry lists.
    int mPageNumber = 0;

    /// Number of the page frame in the physical memory that contains this page.
    int mFrameNumber = 0; 

    

public:
    
        /**
         * Getters and Setters
         * Sorry I got lazy....
         */
        bool IsValid() { return mValidBit; }
        bool WasReferenced() { return mReferenceBit; }
        bool IsModified() { return mModifiedBit; }
        int GetFrameNumber() { return mFrameNumber;}

        void SetValidBit(bool v) { mValidBit = v; }
        void SetReferenceBit(bool r) { mReferenceBit = r; }
        void SetModifiedBit(bool m) { mModifiedBit = m; }
        void SetFrameNumber(int f) { mFrameNumber = f; }


        /**
         * Constructor. */ 
        PageTableEntry() {}

        /**
         * Constructor. 
        PageTableEntry(PageReplacementSystem * pRSystem) { mPageReplacementSystem = pRSystem; }
        
        /**
         * Destructor. */ 
        ~PageTableEntry() {}

        /**
         * Short cut to printing out a table entry.
         * @param out The output stream we are printing to.
         * @param entry The table entry we are printing.
         * @returns The output stream.
         */
        friend std::ostream& operator<<(std::ostream& out, PageTableEntry & entry)
        {
            // Get the amount of hex characters that the frame number will
            // take up.
            int frameSpace = 2;


            out << entry.mValidBit << " "; 
            out << entry.mReferenceBit << " ";
            out << entry.mModifiedBit << " ";
            out << std::setfill('0') << std::setw(frameSpace) << entry.mFrameNumber;

            return out;
        }

        

   
};


class PageTable
{
private:
    /// Virtual Memory System that owns this
    VirtualMemorySystem * mVMSystem;

    /// Amount of frames in the physical memory. 
    int mFrameCount;

    /// Size of each page, in bytes.
    int mPageSize = 0;


    /// Amount of entries the table can fit.
    int mTableSize;

    /// Array of page table entries.
    PageTableEntry * mTableEntries;

    /// Pointer to the PageReplacementSystem the entries will work with.
    PageReplacementSystem * mPageReplacementSystem;

public:

    PageTable(int tableSize, int frameCount, PageReplacementSystem * vm);
    ~PageTable();

    /**
     * Short cut to printing out this table.
     * @param out The output stream we are printing to.
     * @param table The table we are printing.
     * @returns The output stream.
     */
    friend std::ostream& operator<<(std::ostream& out, PageTable & table)
    {
        // Get the amount of hex characters that the entry index will
        // take up.
        int indexSpace = (table.mTableSize - 1) / 16 + 1;

        // The table starts with a new line...
        out << std::endl;

        // Print out a table header.
        out << "P V R M F" << std::endl;
        out << "__________" << std::endl;
        
        // Use hex for integers, and numbers for booleans.
        out << std::hex << std::noboolalpha;

        // Print out each table entry 
        for (int i = 0; i < table.mTableSize; i++)
        {
            out << std::setfill('0') << std::setw(indexSpace) << i << " ";
            out << table.mTableEntries[i] << std::endl;
        }

        // Set it back to normal.
        out << std::dec << std::boolalpha << std::endl; 
        

        return out;
    }

    PageTableEntry * EntryAt(int index);

    
};

/**
 * Class that is in charge of placing, removing, and replacing
 * pages into the memory.
 */
class PageReplacementSystem
{
private:
    /// The virtual memory manager that owns this.
    VirtualMemorySystem * mVMS;

    /// The table this system is managing.
    PageTable * mPageTable;
    
    /// The amount of frames allocated to the process whose 
    /// table we are managing.
    int mFramesAllocated;

protected:
    /**
     * Pick a page to remove the frame from using the system's Replacement
     * algorithm
     * @returns The victim page.
     */
    virtual PageTableEntry * PickVictim() = 0;

    virtual void LoadFrame(PageTableEntry * entry, int frame);
    
    /// Map contaning the numbers of the frames the proccess
    /// will be using as keys and stores whether they are in use. 
    std::map<int, PageTableEntry*> mFrames;

    /** 
     * Get the maxiumum amount of frames that can be allocated to the process.
     * @returns mFramesAllocated variable. */
    int GetMaxFrameCount() { return mFramesAllocated; }
public:
    PageReplacementSystem(int framesAllocated);
    ~PageReplacementSystem();
    void SetVM(VirtualMemorySystem * vm) { mVMS = vm; mPageTable = vm->GetPageTable(); }

    void ReplaceFrame(PageTableEntry * entry);
};


/**
 * FIFO replacement system
 */
 class FIFO : public PageReplacementSystem
 {
private:

    virtual PageTableEntry * PickVictim() override;

    virtual void LoadFrame(PageTableEntry * entry, int frame) override;

    /// FIFO data structure used to pick a victim 
    std::queue<PageTableEntry*> mVictimQueue;
public:
    FIFO(int frames) : PageReplacementSystem(frames) {}
    ~FIFO() {}

 };

/**
 * Clock replacment system.
 */
class Clock : public PageReplacementSystem
{
    virtual PageTableEntry * PickVictim() override;

    virtual void LoadFrame(PageTableEntry * entry, int frame) override;

    /// Circular buffer that will keep track of pages currently 
    /// loaded into frames.
    PageTableEntry ** mBuffer;


    /// Current position in the clock buffer. 
    int mHand;

    /** 
     * Move the hand forward one element.
     */
     void MoveHand()
     {
         mHand++;
         if (mHand >= GetMaxFrameCount())
            mHand -= GetMaxFrameCount();
     } 
public:
    Clock(int frames);
    ~Clock() { delete[] mBuffer; }

};
/**
 * Constructor.
 * @param pMemory Amount of physical memory in the system, in bytes.
 * @param pageFrames Amount of page frames the memory is divided into, in bytes.
 * @param logicalAddessSize The size of the logical address, in bits. 
 * @param printInterval Amount of commands between each printing of the page table.
 */
VirtualMemorySystem::VirtualMemorySystem(long pMemory, int pageFrames, int logicalAddressSize, int printInterval, PageReplacementSystem * pRSystem)
{
    mPhysicalMemorySize = pMemory;
    mFrameCount = pageFrames;
    mLogicalAddressSize = logicalAddressSize;
    mTablePrintInterval = printInterval;
    if (mLogicalAddressSize > 0 )
    {
        // Virtual address space is 2^(mLogicalAddressSize)
        mVirtualAddressSpace  = 0x1 << (mLogicalAddressSize);
    }
    else 
    {
        std::cerr << "Logical address length must be at least 1 bit.";
        exit(-1);
    }

    if (mFrameCount <= 0)
    {
        std::cerr << "Page Frame count must be at least 1.";
        exit(-2);
    }

    mPageSize = mPhysicalMemorySize / mFrameCount;
    
    
    mPageReplacementSystem = pRSystem; 
    mPageReplacementSystem->SetVM(this); 
    mPageTable = new PageTable(mVirtualAddressSpace / mPageSize, mFrameCount, mPageReplacementSystem);

    mOffsetBitSize = (int) log2(mPageSize); 

    mOffsetBitMask = 1;
    
    for (int i = 0; i < mOffsetBitSize - 1; i++)
    {
        mOffsetBitMask = mOffsetBitMask << 1;
        mOffsetBitMask++;
    }
    
    // Print the table at the beginning.
    cout << * mPageTable;
}


/**
 * Destructor. */
 VirtualMemorySystem::~VirtualMemorySystem()
 {
     // Only print the table if it was not just printed.
     if (mTablePrintInterval == 0 || (mReadCount + mWriteCount) % mTablePrintInterval != 0)
     {
         // Print the table one last time before deleting it.
          cout << * mPageTable;

     }

     std::cout << "Total memory references: " << (mReadCount + mWriteCount) << std::endl;
     std::cout << "Total read operations: " << mReadCount << std::endl;
     std::cout << "Total write operations: " << mWriteCount << std::endl;
     delete mPageTable;
 }

/**
 * Perform a read/write command.
 * @param commandType Determines which command to perform.
 * 'R' is read, 'W' is write.
 * @param address The logical address the program is requesting
 * access to. 
 */
 void VirtualMemorySystem::DoCommand(char commandType, int address)
 {
     // Get the offset value.
     int offset = address & mOffsetBitMask;
     
     // Get the page number
     int page = address >> mOffsetBitSize;
     std::cout << std::setfill('0') << std::hex;
     std::cout << commandType << " "<< std::setw(4) << address << " ";
     std::cout << page << " " << std::setw(3) << offset;

     PageTableEntry * entry = mPageTable->EntryAt(page);

     while (!entry->IsValid())
     {
         cout << " F";
         mPageReplacementSystem->ReplaceFrame(entry);
     }

     // We need to offset the frame number because it is going
     // to be at the front of the address. 
     int frameNumber = entry->GetFrameNumber() << mOffsetBitSize;
     
     // Mark that we have referenced this page. 
     entry->SetReferenceBit(true);

     int physicalAddress = frameNumber + offset;

     std::cout << " " << std::setw(5) << physicalAddress << std::endl;

     switch (commandType)
     {
     case 'R': 
        ReadCommand(page, offset);
        break;
     case 'W':
        WriteCommand(page, offset);
        entry->SetModifiedBit(true);
        break;
     default:
        break;

     }
     if (mTablePrintInterval > 0 && (mReadCount + mWriteCount) % mTablePrintInterval  == 0)
     {
         cout << *mPageTable;
     }
 }

 /**
  * Perform a write command. 
  * @param page Virtual page we are accessing.
  * @param offset Offset on the page.
  */
void VirtualMemorySystem::WriteCommand(int page, int offset)
{
    mWriteCount++;
}

 /**
  * Perform a read command. 
  * @param page Virtual page we are accessing.
  * @param offset Offset on the page.
  */
void VirtualMemorySystem::ReadCommand(int page, int offset)
{
    mReadCount++;
}



 /**
  * Constructor.
  * @param tableSize The amount of entries the table can contain.
  * @param frameCount The amount of frames in the physical memory. 
  * @param pRSystem the PageReplacementSystem the table will work with.
  */
PageTable::PageTable(int tableSize, int frameCount, PageReplacementSystem * pRSystem) 
{
    mTableSize = tableSize; 
    mFrameCount = frameCount;
    mTableEntries = new PageTableEntry[mTableSize];
    mPageReplacementSystem = pRSystem;
}

/**
 * Destructor. */
PageTable::~PageTable()
{
    delete[] mTableEntries;
}

/**
 * Get the table entry at the specified index.
 * @param index The index of the table entry.
 * @returns Pointer to the table entry or nullptr if out of range.
 */
PageTableEntry * PageTable::EntryAt(int index)
{ 
    if (index >= 0 && index < mTableSize)
        return mTableEntries + index;
    return nullptr;
}

/**
 * Constructor
 * @param framesAllocated The frames allocated to the proccess.
 */
 PageReplacementSystem::PageReplacementSystem(int framesAllocated)
 {
     mFramesAllocated = framesAllocated;
     for (int i = 0; i < mFramesAllocated; i++)
     {
         mFrames[i + FIRST_FRAME_NUMBER] = nullptr;
     }
 }

/**
 * Destructor.
 */
PageReplacementSystem::~PageReplacementSystem() 
{
}


/**
 * Load a frame into a page table entry.
 * @param entry The entry we are loading the frame into.
 * @param frame The number of the frame we are loading into the entry.
 */
void PageReplacementSystem::LoadFrame(PageTableEntry * entry, int frame)
{
    mFrames[frame] = entry;
    entry->SetFrameNumber(frame);
    entry->SetValidBit(true);

    // Clear modified bit, since this frame was just loaded, it 
    // could not have been modified yet. 
    entry->SetModifiedBit(false);
}

/**
 * Allocate a frame to a table entry
 * @param entry The table entry we are loading a frame for.
 */
void PageReplacementSystem::ReplaceFrame(PageTableEntry *entry )
{
    bool frameLoaded = false;
    for (std::map<int, PageTableEntry*>::iterator iter = mFrames.begin();  iter != mFrames.end() && !frameLoaded; ++iter)
    {
        if (iter->second == nullptr)
        {
            LoadFrame(entry, iter->first);

            frameLoaded = true;
        }   
    }

    if (!frameLoaded)
    {
        PageTableEntry * victim = PickVictim();
        victim->SetValidBit(false);

        // If the victim page is modified, write-back.
        if (victim->IsModified())
        {
            std::cout << " WB";
        }

        LoadFrame(entry, victim->GetFrameNumber());
    }

}

/** Load a frame into a page table entry. 
 * Add the entry into the FIFO queue.
 * @param entry The entry we are loading the frame into.
 * @param frame The number of the frame we are loading into the entry.
 */
void FIFO::LoadFrame(PageTableEntry * entry, int frame)
{
    PageReplacementSystem::LoadFrame(entry, frame);
    mVictimQueue.push(entry);
}


/** 
 * Pick a victim page to unload so that a new page can be loaded.
 * @returns The victim page.
 */
PageTableEntry * FIFO::PickVictim()
{
    PageTableEntry *entry =  mVictimQueue.front();
    mVictimQueue.pop();
    return entry;
}


/**
 * Constructor.
 * @param frames The frames allocated to the proccess.
 */
Clock::Clock(int frames) : PageReplacementSystem(frames)
{
    mBuffer = new PageTableEntry*[frames];
}


/** Load a frame into a page table entry. 
 * @param entry The entry we are loading the frame into.
 * @param frame The number of the frame we are loading into the entry.
 */
void Clock::LoadFrame(PageTableEntry * entry, int frame)
{
    PageReplacementSystem::LoadFrame(entry, frame);
    mBuffer[mHand] = entry;
    MoveHand();
}


/** 
 * Pick a victim page to unload so that a new page can be loaded.
 * @returns The victim page.
 */
PageTableEntry * Clock::PickVictim()
{
    while (mBuffer[mHand]->WasReferenced())
    {
        mBuffer[mHand]->SetReferenceBit(false);
        MoveHand();
    }
    return mBuffer[mHand];
}

int main(int argc, char** argv)
{
    VirtualMemorySystem vMSystem = VirtualMemorySystem(PHYSICAL_MEMORY_SIZE, FRAME_COUNT, LOGICAL_ADDRESS_LENGTH, atoi(argv[1]), new Clock(3));
    std::ifstream inputFile(argv[2]);
    
    while(!inputFile.eof())
    {
        char commandType;
        int address;


        inputFile >> commandType;
        inputFile >> std::hex >> address;
        inputFile >> std::dec;
        
        vMSystem.DoCommand(commandType, address);
    }
    return 0;
}

