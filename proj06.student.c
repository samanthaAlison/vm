#include <string>
#include <vector>

#include <iostream>
using std::cin; using std::cout; using std::cerr; using std::endl;
#include <iomanip>
#include <fstream>
#include <sstream>


#include <cmath>

const long PHYSICAL_MEMORY_SIZE = 1048576;
const int FRAME_COUNT = 256;
const int LOGICAL_ADDRESS_LENGTH = 16;


class PageTable;

class VirtualMemorySystem
{
private:

    void ReadCommand(int page, int offset);
    void WriteCommand(int page, int offset);
    
    /// Amount of commands between each table print.
    /// A value of 0 means don't ever pint the table.
    int mTablePrintInterval = 0;

    /// Read command count
    int mReadCount = 0;
    /// Write command count
    int mWriteCount = 0;

    /// Physical memory we are working with, in bytes.
    long mPhysicalMemorySize;

    /// Memory address space we promise to each program, in bytes.
    long mVirtualAddressSpace;

    /// Number of page frames
    int mFrameCount;

    /// Logical address size, in bits. 
    long mLogicalAddressSize;

    /// Size of each page, in bytes.
    int mPageSize = 0;

    /// Page table that this system manages.
    PageTable * mPageTable;
    
    /// Amount of bits used for the offset in virtual memory addresses.
    int mOffsetBitSize;
    
    /// Bit mask to use to extract the offset from a command.
    int mOffsetBitMask;

public:
    VirtualMemorySystem() {}
    VirtualMemorySystem(long pMemory, int pageFrames, int logicalAddressSize, int printInterval);
    ~VirtualMemorySystem();

    void DoCommand(char, int);


};

/// Class that describes an entry in the page table.
class PageTableEntry
{
private:
    /// Flag that is set to indicate that this entry is valid.
    bool mValidBit = false;

    /// Flag that is set to indicate that this page that this entry represents has been 
    /// referenced.
    bool mReferenceBit = false;

    /// Flag that is set when the page this entry references has been modified. 
    bool mModifiedBit = false;


    /// Number of the of the virtual page that this table entry lists.
    int mPageNumber = 0;

    /// Number of the page frame in the physical memory that contains this page.
    int mFrameNumber = 0; 


public:
    
        /**
         * Constructor. */ 
        PageTableEntry() {}
        
        /**
         * Destructor. */ 
        ~PageTableEntry() {}

        /**
         * Short cut to printing out a table entry.
         * @param out The output stream we are printing to.
         * @param entry The table entry we are printing.
         * @returns The output stream.
         */
        friend std::ostream& operator<<(std::ostream& out, PageTableEntry & entry)
        {
            // Get the amount of hex characters that the frame number will
            // take up.
            int frameSpace = 2;


            out << entry.mValidBit << " "; 
            out << entry.mReferenceBit << " ";
            out << entry.mModifiedBit << " ";
            out << std::setfill('0') << std::setw(frameSpace) << entry.mFrameNumber;

            return out;
        }

        

   
};


class PageTable
{
private:
    /// Virtual Memory System that owns this
    VirtualMemorySystem * mVMSystem;

    /// Amount of frames in the physical memory. 
    int mFrameCount;

    /// Size of each page, in bytes.
    int mPageSize = 0;


    /// Amount of entries the table can fit.
    int mTableSize;

    /// Array of page table entries.
    PageTableEntry * mTableEntries;

public:

    PageTable(int tableSize, int frameCount);
    ~PageTable();

    /**
     * Short cut to printing out this table.
     * @param out The output stream we are printing to.
     * @param table The table we are printing.
     * @returns The output stream.
     */
    friend std::ostream& operator<<(std::ostream& out, PageTable & table)
    {
        // Get the amount of hex characters that the entry index will
        // take up.
        int indexSpace = (table.mTableSize - 1) / 16 + 1;

        // The table starts with a new line...
        out << std::endl;

        // Print out a table header.
        out << "P V R M F" << std::endl;
        out << "__________" << std::endl;
        
        // Use hex for integers, and numbers for booleans.
        out << std::hex << std::noboolalpha;

        // Print out each table entry 
        for (int i = 0; i < table.mTableSize; i++)
        {
            out << std::setfill('0') << std::setw(indexSpace) << i << " ";
            out << table.mTableEntries[i] << std::endl;
        }

        // Set it back to normal.
        out << std::dec << std::boolalpha << std::endl; 
        

        return out;
    }

    
};



/**
 * Constructor.
 * @param pMemory Amount of physical memory in the system, in bytes.
 * @param pageFrames Amount of page frames the memory is divided into, in bytes.
 * @param logicalAddessSize The size of the logical address, in bits. 
 * @param printInterval Amount of commands between each printing of the page table.
 */
VirtualMemorySystem::VirtualMemorySystem(long pMemory, int pageFrames, int logicalAddressSize, int printInterval)
{
    mPhysicalMemorySize = pMemory;
    mFrameCount = pageFrames;
    mLogicalAddressSize = logicalAddressSize;
    mTablePrintInterval = printInterval;
    if (mLogicalAddressSize > 0 )
    {
        // Virtual address space is 2^(mLogicalAddressSize)
        mVirtualAddressSpace  = 0x1 << (mLogicalAddressSize);
    }
    else 
    {
        std::cerr << "Logical address length must be at least 1 bit.";
        exit(-1);
    }

    if (mFrameCount <= 0)
    {
        std::cerr << "Page Frame count must be at least 1.";
        exit(-2);
    }

    mPageSize = mPhysicalMemorySize / mFrameCount;
    
    mPageTable = new PageTable(mVirtualAddressSpace / mPageSize, mFrameCount);

    mOffsetBitSize = (int) log2(mPageSize); 

    mOffsetBitMask = 1;
    
    for (int i = 0; i < mOffsetBitSize - 1; i++)
    {
        mOffsetBitMask = mOffsetBitMask << 1;
        mOffsetBitMask++;
    }
    
    // Print the table at the beginning.
    cout << * mPageTable;
}


/**
 * Destructor. */
 VirtualMemorySystem::~VirtualMemorySystem()
 {
     // Only print the table if it was not just printed.
     if (mTablePrintInterval == 0 || (mReadCount + mWriteCount) % mTablePrintInterval != 0)
     {
         // Print the table one last time before deleting it.
          cout << * mPageTable;

     }

     std::cout << "Total memory references: " << (mReadCount + mWriteCount) << std::endl;
     std::cout << "Total read operations: " << mReadCount << std::endl;
     std::cout << "Total write operations: " << mWriteCount << std::endl;
     delete mPageTable;
 }

/**
 * Perform a read/write command.
 * @param commandType Determines which command to perform.
 * 'R' is read, 'W' is write.
 * @param address The logical address the program is requesting
 * access to. 
 */
 void VirtualMemorySystem::DoCommand(char commandType, int address)
 {
     // Get the offset value.
     int offset = address & mOffsetBitMask;
     
     // Get the page number
     int page = address >> mOffsetBitSize;
     std::cout << std::setfill('0') << std::hex;
     std::cout << commandType << " "<< std::setw(4) << address << " ";
     std::cout << page << " " << std::setw(3) << offset << std::endl;

     switch (commandType)
     {
     case 'R': 
        ReadCommand(page, offset);
        break;
     case 'W':
        WriteCommand(page, offset);
        break;
     default:
        break;

     }
     if (mTablePrintInterval > 0 && (mReadCount + mWriteCount) % mTablePrintInterval  == 0)
     {
         cout << *mPageTable;
     }
 }

 /**
  * Perform a write command. 
  * @param page Virtual page we are accessing.
  * @param offset Offset on the page.
  */
void VirtualMemorySystem::WriteCommand(int page, int offset)
{
    mWriteCount++;
}

 /**
  * Perform a read command. 
  * @param page Virtual page we are accessing.
  * @param offset Offset on the page.
  */
void VirtualMemorySystem::ReadCommand(int page, int offset)
{
    mReadCount++;
}

 /**
  * Constructor.
  * @param tableSize The amount of entries the table can contain.
  * @param frameCount The amount of frames in the physical memory. 
  */
PageTable::PageTable(int tableSize, int frameCount) 
{
    mTableSize = tableSize; 
    mFrameCount = frameCount;
    mTableEntries = new PageTableEntry[mTableSize];
}

/**
 * Destructor. */
PageTable::~PageTable()
{
    delete[] mTableEntries;
}


int main(int argc, char** argv)
{
    VirtualMemorySystem vMSystem = VirtualMemorySystem(PHYSICAL_MEMORY_SIZE, FRAME_COUNT, LOGICAL_ADDRESS_LENGTH, atoi(argv[1]));
    std::ifstream inputFile(argv[2]);
    
    while(!inputFile.eof())
    {
        char commandType;
        int address;


        inputFile >> commandType;
        inputFile >> std::hex >> address;
        inputFile >> std::dec;
        
        vMSystem.DoCommand(commandType, address);
    }
    return 0;
}

